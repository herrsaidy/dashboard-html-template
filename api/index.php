<?php

// php -S 127.0.0.1:9000
// http://127.0.0.1:9000/dist/html/template.html

function getDb()
{
    $json = file_get_contents(__DIR__ . '/db.json');

    return json_decode($json, true);
}
function storeDb($data)
{
    file_put_contents(__DIR__ . '/db.json', json_encode($data, JSON_PRETTY_PRINT));
}

if ($_GET['a'] === 'get_config')
{
    $config = getDb();

    $config['contentSources'] = [];
    foreach ($config['grid'] ?? [] as $i => $grid) {
        foreach ($grid['contents'] ?? [] as $type) {
            $config['contentSources'][$type] = file_get_contents(__DIR__ . '/' . $type . '.html');
        }
    }

    echo json_encode($config);
    exit;
}

if ($_GET['a'] === 'store_config')
{
    $config = getDb();
    if (!isset($config['grid'])) {
        $config['grid'] = [];
    }

    $config['grid'][] = [
        'grid' => $_POST['grid'],
        'contents' => explode(',', $_POST['contents'])
    ];

    storeDb($config);
    exit;
}
