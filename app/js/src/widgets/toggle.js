class Toggle extends Widget {
    constructor(node) {
        super(node, '.js-toggle');

        this.$button = this.queryElement('.button');
        this.onButtonClick = this.onButtonClick.bind(this);

        this.init();
    }

    build() {
        this.$button.addEventListener('click', this.onButtonClick.bind(this));
    }

    onButtonClick(e) {
        e.preventDefault();

        this.$node.classList.toggle('_opened');
    }

    static init(el) {
        el && new Toggle(el);
    }
}

document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('.js-toggle').forEach(item => Toggle.init(item));
});
